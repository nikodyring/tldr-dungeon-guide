# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v1.1.1...v1.2.0) (2020-10-05)


### Features

* **commands/covenant:** add covenant base command ([825a4f7](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/825a4f7a38123f87661e78746a76281fb7f855c1))

### [1.1.1](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v1.1.0...v1.1.1) (2020-10-01)

## [1.1.0](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v1.0.0...v1.1.0) (2020-10-01)


### Features

* **dungeon/theater-of-pain:** added Theater of Pain + links in it ([58f28b5](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/58f28b5e0a63b41f7d62bff380c6abdfc5945450))
* **dungeons/spires-of-ascension:** add link to Dark lance ([14e3083](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/14e30832ae3ecd568b2979346de289a751a409d1))

## [1.0.0](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v0.1.5...v1.0.0) (2020-09-27)


### Features

* **dungeons/sanguine-depths:** add missing links ([b9f26dc](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/b9f26dc8f9c4236defdabcc454eee13b75d0d445))

### [0.1.5](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v0.1.4...v0.1.5) (2020-09-27)


* **gitlab:** build only tags ([13a325a](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/13a325abf39c1f6abc44f43f040926ff67665276))


### Other

* **package.json:** add ci to changelog generation ([b6d65b0](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/b6d65b005b49e17dff2cac0cb494f35c27bfb471))

### [0.1.4](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v0.1.3...v0.1.4) (2020-09-27)


### Features

* **dungeons/sanguine-depths:** add more links to spells ([ddae6ec](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/ddae6ec12f982fe70202ca969b2950e2de31d844))

### [0.1.3](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v0.1.2...v0.1.3) (2020-09-27)


### Other

* **package.json:** add standard-version type config ([d5512b5](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/d5512b53085d781c57738914a1efe8be342f227d))

### [0.1.2](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v0.1.1...v0.1.2) (2020-09-27)


### Features

* **dungeons/sanguine-depths:** add link to hungering drain ([e9d4cba](https://gitlab.com/nikodyring/tldr-dungeon-guide/commit/e9d4cba03128a8741ce12b2f3eb7072397fb53fd))

### [0.1.1](https://gitlab.com/nikodyring/tldr-dungeon-guide/compare/v0.1.0...v0.1.1) (2020-09-27)

## 0.1.0 (2020-09-27)
